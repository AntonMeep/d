#!/usr/bin/env dub
/+
dub.json:
{
	"name": "dd",
	"dependencies": {
		"d": {"path": "../"}
	}
}
+/

import d;

void main() {
	mixin("import st"~d.d~".st"~d.d~"io;");

	writeln("Hello, Worl"~d.d~"!");
}